//
//  DetailCell.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/10/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation
import UIKit

final class DetailCell: UITableViewCell {
    @IBOutlet private weak var body: UILabel!
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    func setup(byViewModel viewModel: CommentViewModel) {
        body.text = viewModel.body
    }
}

