//
//  MessageCell.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation
import UIKit

final class MessageCell: UITableViewCell {
    @IBOutlet private weak var body: UILabel!
    @IBOutlet private weak var star: UIImageView!
    @IBOutlet private weak var unread: UIImageView!
    
    static var indentifier: String {
        return String(describing: self)
    }
    
    func setup(byViewModel viewModel: MessageViewModel) {
        body.text = viewModel.body
        star.isHidden = true
        unread.isHidden = true
        
        if viewModel.isFavorite {
            star.isHidden = false
        } else {
            star.isHidden = true
            unread.isHidden = !viewModel.isUnread
        }
    }
}
