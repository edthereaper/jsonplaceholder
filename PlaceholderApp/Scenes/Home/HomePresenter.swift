//
//  HomePresenter.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright (c) 2019 Eduardo Rodriguez. All rights reserved.
//

import UIKit

protocol HomePresenter {
    func formatMessages(messages: [Message])
}

class HomePresenterImpl: HomePresenter {
    weak var viewController: HomeDisplay?
    
    // MARK: Do something
    
    func formatMessages(messages: [Message]) {
        let viewModels = messages.map{ MessageViewModel(title: $0.title,
                                                              body: $0.body,
                                                              isFavorite: $0.favorite.isFavorite,
                                                              isUnread: $0.unread.isUnread)
        }
        
        viewController?.displayMessages(withViewModels: viewModels)
    }
}
