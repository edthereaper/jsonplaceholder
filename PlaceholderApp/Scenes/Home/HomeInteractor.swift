//
//  HomeInteractor.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright (c) 2019 Eduardo Rodriguez. All rights reserved.
//

import UIKit

protocol HomeInteractor {
    func retriveMessages()
    func retriveMessage(withIndex index: Int) -> Message?
    func removeMessage(withIndex index: Int)
    func filterFavoritesMessages()
    func removeAll()
}

final class HomeInteractorImpl: HomeInteractor, LocatorDependency {
    struct Dependencies: LocatorInjector {
        let getMessages: GetMessages = inject()
        let removeMessages: RemoveMessages = inject()
    }
    
    let dependencies: Dependencies
    var presenter: HomePresenter?
    var lastMessages: [Message] = []
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func retriveMessages() {
        dependencies.getMessages.execute { [weak self] (messages) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.presenter?.formatMessages(messages: messages)
            strongSelf.lastMessages = messages
        }
    }
    
    func filterFavoritesMessages() {
        let filterMessages = self.lastMessages.filter { $0.favorite.isFavorite }
        presenter?.formatMessages(messages: filterMessages)
    }
    
    func retriveMessage(withIndex index: Int) -> Message? {
        guard lastMessages.count > 0 else {
            return nil
        }
        
        return lastMessages[index]
    }
    
    func removeMessage(withIndex index: Int) {
        guard lastMessages.count > 0 else {
            return
        }
        
        dependencies.removeMessages.execute(message: lastMessages[index])
        lastMessages.remove(at: index)
    }

    func removeAll() {
        dependencies.removeMessages.execute(messages: lastMessages)
        lastMessages.removeAll()
    }
}
