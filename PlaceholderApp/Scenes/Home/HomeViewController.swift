//
//  HomeViewController.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright (c) 2019 Eduardo Rodriguez. All rights reserved.
//

import UIKit

protocol HomeDisplay: class {
    func displayMessages(withViewModels viewmodels:[MessageViewModel])
}

class HomeViewController: UIViewController {
    var viewModels: [MessageViewModel] = []
    var interactor: HomeInteractor?
    
    // MARK: Object lifecycle
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var button: UIButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup() {
        let viewController = self
        let interactor = HomeInteractorImpl()
        let presenter = HomePresenterImpl()
        viewController.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = viewController
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let message = sender as? Message, let scene = segue.identifier, scene == String(describing: DetailsViewController.self), let screen = segue.destination as? DetailsViewController {
            screen.message = message
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Posts"
        self.view.backgroundColor = UIColor.white
        setupRightButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.retriveMessages()
    }
    
    @IBAction func didTapRemoveAll(_ sender: Any) {
        UIView.animate(withDuration: 1.0, animations: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.loader.alpha = 0.0
            strongSelf.tableView.alpha = 0.0
        }) { [weak self](finished) in
            guard let strongSelf = self, finished else {
                return
            }
            strongSelf.interactor?.removeAll()
            strongSelf.viewModels.removeAll()
            strongSelf.tableView.reloadData()
        }
        
    }
    
    @IBAction func didTapSegmentedControl(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            interactor?.retriveMessages()
        default:
            interactor?.filterFavoritesMessages()
        }
    }
}

extension HomeViewController: HomeDisplay {
    func displayMessages(withViewModels viewmodels:[MessageViewModel]) {
        self.viewModels = viewmodels
        loader.alpha = 1.0
        tableView.alpha = 1.0
        tableView.isHidden = false
        tableView.reloadData()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: MessageCell = tableView.dequeueReusableCell(withIdentifier: MessageCell.indentifier, for: indexPath) as? MessageCell else {
            return UITableViewCell()
        }
        cell.setup(byViewModel: viewModels[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { [weak self] (action, indexPath) in
            // delete item at indexPath
            self?.interactor?.removeMessage(withIndex: indexPath.row)
            self?.viewModels.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        delete.backgroundColor = UIColor.red
        
        return [delete]
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard Reachability.isConnectedToNetwork() else {
            self.alert(withViewModel: AlertViewModel.notInternetConnection)
            return
        }
        self.performSegue(withIdentifier: String(describing: DetailsViewController.self),
                          sender: interactor?.retriveMessage(withIndex: indexPath.row))
    }
}

private extension HomeViewController {
    func setupRightButton() {
        button = UIButton(type: .custom)
        button.setImage(UIImage(named: "reload-icon"), for: .normal)
        button.addTarget(self, action: #selector(didTapReloadButton), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    @objc func didTapReloadButton() {
        loader.alpha = 1.0
        tableView.isHidden = true
        segmentedControl.selectedSegmentIndex = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.interactor?.retriveMessages()
        }
        
    }
}
