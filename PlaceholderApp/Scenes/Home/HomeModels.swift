//
//  HomeModels.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright (c) 2019 Eduardo Rodriguez. All rights reserved.
//

import UIKit

typealias Message = HomeModel.Message
typealias Favorite = HomeModel.Favorite
typealias Unread = HomeModel.Unread

typealias MessageViewModel = HomeViewModel.Message

enum HomeModel {
    struct Message {
        let userId: Int
        let iden: Int
        let title: String
        let body: String
        var favorite: Favorite
        var unread: Unread
        
        init(userId: Int, iden: Int, title: String, body: String) {
            self.userId = userId
            self.iden = iden
            self.title = title
            self.body = body
            self.favorite = Favorite(iden: iden, isFavorite: false)
            self.unread = Unread(iden: iden, isUnread: false)
        }

        init(userId: Int, iden: Int, title: String, body: String, favorite: Favorite, unread: Unread) {
            self.userId = userId
            self.iden = iden
            self.title = title
            self.body = body
            self.favorite = favorite
            self.unread = unread
        }
    }
    
    struct Favorite {
        let iden: Int
        let isFavorite: Bool
    }
    
    struct Unread {
        let iden: Int
        let isUnread: Bool
    }
}

enum HomeViewModel {
    struct Message {
        let title: String
        let body: String
        let isFavorite: Bool
        let isUnread: Bool
    }
}




