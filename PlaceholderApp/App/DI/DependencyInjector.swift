//
//  DependencyInjector.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

final class Locator {
    static let shared = Locator()
    
    var getMessage: GetMessages {
        return GetMessagesImpl()
    }
    
    var requestMessages: RequestMessages {
        return RequestMessagesImpl()
    }
    
    var cacheMessages: CacheMessages {
        return CoreDataMessageImpl()
    }
    
    var cacheFavorites: CacheFavorites {
        return CoreDataFavoriteImpl()
    }
    
    var setFavorite: SetFavoriteStatus {
        return SetFavoriteStatusImpl()
    }
    
    var getFavorite: GetFavoriteStatus {
        return GetFavoriteStatusImpl()
    }
    
    var cacheUnread: CacheUnread {
        return CoreDataUnreadImpl()
    }
    
    var setUnreadStatus: SetUnreadStatus {
        return SetUnreadStatusImpl()
    }
    
    var getUnreadStatus: GetUnreadStatus {
        return GetUnreadStatusImpl()
    }
    
    var removeMessage: RemoveMessages {
        return RemoveMessageImpl()
    }
    
    var requestUserData: RequestUser {
        return RequestUserImpl()
    }
    
    var requestCommentData: RequestComment {
        return RequestCommmentsImpl()
    }
}

protocol LocatorDependency: Dependable where Dependencies: LocatorInjector {}
protocol LocatorInjector: Injectable {}

extension LocatorInjector {
    private static var injector: Locator {
        return Locator.shared
    }
    
    static func inject() -> GetMessages {
        return injector.getMessage
    }
    
    static func inject() -> RequestMessages {
        return injector.requestMessages
    }

    static func inject() -> CacheMessages {
        return injector.cacheMessages
    }
    
    static func inject() -> CacheFavorites {
        return injector.cacheFavorites
    }
    
    static func inject() -> SetFavoriteStatus {
        return injector.setFavorite
    }
    
    static func inject() -> GetFavoriteStatus {
        return injector.getFavorite
    }
    
    static func inject() -> CacheUnread {
        return injector.cacheUnread
    }
    
    static func inject() -> SetUnreadStatus {
        return injector.setUnreadStatus
    }
    
    static func inject() -> GetUnreadStatus {
        return injector.getUnreadStatus
    }
    
    static func inject() -> RemoveMessages {
        return injector.removeMessage
    }
    
    static func inject() -> RequestUser {
        return injector.requestUserData
    }
    
    static func inject() -> RequestComment {
        return injector.requestCommentData
    }
}


