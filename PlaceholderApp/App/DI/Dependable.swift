//
//  Dependable.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

protocol Injectable {
    init()
}

protocol Dependable {
    associatedtype Dependencies: Injectable
    var dependencies: Dependencies { get }
    
    init(dependencies: Dependencies)
}

extension Dependable {
    init() {
        self.init(dependencies: Dependencies())
    }
}
