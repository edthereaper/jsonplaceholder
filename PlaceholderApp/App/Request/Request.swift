//
//  Request.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation
import SystemConfiguration

enum ResponseStatus: Int {
    case success
    case failure
}

typealias ResponseData = [String: AnyObject]
typealias ResponseDataArr = [ResponseData]
typealias RequestCallback = ((ResponseStatus, ResponseDataArr) -> Void)?

final class Request {
    let defaultSession = URLSession(configuration: .default)
    
    func get(url urlString: String, completion: RequestCallback) {
        guard let url = URL(string: urlString) else {
            print("[ERROR]: \(urlString)")
            completion?(.failure, [])
            return
        }
        let urlRequest = URLRequest(url: url)
        defaultSession.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data  else {
                print("[ERROR]: \(String(describing: error?.localizedDescription))")
                completion?(.failure, [])
                return
            }
            
            let jsonData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments)
            completion?(.success, jsonData as? ResponseDataArr ?? [])
        }.resume()
    }
}
