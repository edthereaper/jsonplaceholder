//
//  CoreDataMessageImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataMessageImpl: CoreDataProtocol, CacheMessages {
    typealias Object = Message
    typealias CoreDataEntity = NSManagedObject

    func retriveMessages() -> [Message] {
        let storedMessages = retrieveAll(inside: .message)
        let messages = storedMessages.compactMap({ $0 as? MessagePersistence }).map({ $0.toMessage() })
        
        return messages
    }
    
    func retriveMessages(byMessageId messageId: Int) -> [Message] {
        let storedMessages = retrieve(byPredicate: NSPredicate(format: "id == %i", messageId), inside: .message)
        let messages = storedMessages.compactMap({ $0 as? MessagePersistence }).map({ $0.toMessage() })
        
        return messages
    }
    
    func saveOrUpdate(message: Message) {
        guard
            let managedContext = self.managedContext,
            let entityDescription = NSEntityDescription.entity(forEntityName: CoreDataTable.message.rawValue, in: managedContext)
        else {
                return
        }
        if var messageFound = retrieve(byPredicate: NSPredicate(format: "id == %i", message.iden), inside: .message).first {
            update(&messageFound, withMessage: message)
        } else {
            save(message: message,
                 usingEntityDescriptor: entityDescription,
                 andManagedContext: managedContext)
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("[ERROR] \(#function) \(error.description)")
        }
    }
    
    func dropMessages() {
        removeAll(inside: .message)
    }
    
    func removeMessage(withIden iden: Int) {
        remove(byPredicate: NSPredicate(format: "id == %i", iden),
               inside: .message)
    }
    
    func updateFavoriteStatus(to message: Message, with favoriteFlag: Bool) {
        guard let managedContext = self.managedContext else {
            return
        }
        if let messageFound = retrieve(byPredicate: NSPredicate(format: "id == %i", message.iden), inside: .message).first as? MessagePersistence {
            messageFound.isFavorite = favoriteFlag
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("[ERROR] \(#function) \(error.description)")
        }
    }
    
    func updateUnreadStatus(to message: Message, with unreadFlag: Bool) {
        guard let managedContext = self.managedContext else {
            return
        }
        if let messageFound = retrieve(byPredicate: NSPredicate(format: "id == %i", message.iden), inside: .message).first as? MessagePersistence {
            messageFound.isUnread = unreadFlag
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("[ERROR] \(#function) \(error.description)")
        }
    }
}

private extension CoreDataMessageImpl {
    func save(message: Message, usingEntityDescriptor entityDescriptor: NSEntityDescription, andManagedContext managedContext: NSManagedObjectContext) {
        var messageObj = NSManagedObject(entity: entityDescriptor, insertInto: managedContext)
        messageObj.setValue(message.iden, forKey: MessagesParser.id.rawValue)
        update(&messageObj, withMessage: message)
    }
    
    func update(_ messageObj: inout NSManagedObject, withMessage message: Message) {
        messageObj.setValue(message.title, forKey: MessagesParser.title.rawValue)
        messageObj.setValue(message.userId, forKey: MessagesParser.userId.rawValue)
        messageObj.setValue(message.body, forKey: MessagesParser.body.rawValue)
    }
}

extension MessagePersistence {
    func toMessage() -> Message {
        return Message(userId: Int(self.userId),
                       iden: Int(self.id),
                       title: self.title ?? "",
                       body: self.body ?? ""
        )
    }
}
