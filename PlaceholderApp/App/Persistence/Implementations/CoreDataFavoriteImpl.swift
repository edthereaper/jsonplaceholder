//
//  CoreDataFavoriteImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataFavoriteImpl: CoreDataProtocol, CacheFavorites {
    typealias Object = Favorite
    typealias CoreDataEntity = NSManagedObject
    
    func getFavoriteStatus(byMessageId messageId: Int) -> Favorite? {
        let storedFavorites = retrieve(byPredicate: NSPredicate(format:"idMessage == %i", messageId), inside: .favorite)
        let favorite = storedFavorites.compactMap({ $0 as? FavoritePersistence }).map({ $0.toFavorite() }).first
        
        return favorite
    }
    
    func saveOrUpdate(favorite: Favorite) {
        guard
            let managedContext = self.managedContext,
            let entityDescription = NSEntityDescription.entity(forEntityName: CoreDataTable.favorite.rawValue, in: managedContext)
            else {
                return
        }
        if var favoriteFound = retrieve(byPredicate: NSPredicate(format: "idMessage == %i", favorite.iden), inside: .favorite).first {
            update(&favoriteFound, withFavorite: favorite)
        } else {
            save(favorite: favorite,
                 usingEntityDescriptor: entityDescription,
                 andManagedContext: managedContext)
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("[ERROR] \(#function) \(error.description)")
        }
    }
}

private extension CoreDataFavoriteImpl {
    func save(favorite: Favorite, usingEntityDescriptor entityDescriptor: NSEntityDescription, andManagedContext managedContext: NSManagedObjectContext) {
        var favoriteObj = NSManagedObject(entity: entityDescriptor, insertInto: managedContext)
        favoriteObj.setValue(favorite.iden, forKey: FavoriteParser.idMessage.rawValue)
        update(&favoriteObj, withFavorite: favorite)
    }
    
    func update(_ messageObj: inout NSManagedObject, withFavorite favorite: Favorite) {
        messageObj.setValue(favorite.isFavorite, forKey: FavoriteParser.isFavorite.rawValue)
    }
}

extension FavoritePersistence {
    func toFavorite() -> Favorite {
        return Favorite(iden: Int(self.idMessage),
                        isFavorite: self.isFavorite)
    }
}
