//
//  CoreDataUnreadImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataUnreadImpl: CoreDataProtocol, CacheUnread {
    typealias Object = Unread
    typealias CoreDataEntity = NSManagedObject
    
    func getUnreadStatus(byMessageId messageId: Int) -> Unread? {
        let storedFavorites = retrieve(byPredicate: NSPredicate(format:"idMessage == %i", messageId), inside: .unread)
        let unread = storedFavorites.compactMap({ $0 as? UnreadPersistence }).map({ $0.toUnread() }).first
        
        return unread
    }
    
    func saveOrUpdate(unread: Unread) {
        guard
            let managedContext = self.managedContext,
            let entityDescription = NSEntityDescription.entity(forEntityName: CoreDataTable.unread.rawValue, in: managedContext)
            else {
                return
        }
        if var favoriteFound = retrieve(byPredicate: NSPredicate(format: "idMessage == %i", unread.iden), inside: .unread).first {
            update(&favoriteFound, withFavorite: unread)
        } else {
            save(unread: unread,
                 usingEntityDescriptor: entityDescription,
                 andManagedContext: managedContext)
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("[ERROR] \(#function) \(error.description)")
        }
    }
}

private extension CoreDataUnreadImpl {
    func save(unread: Unread, usingEntityDescriptor entityDescriptor: NSEntityDescription, andManagedContext managedContext: NSManagedObjectContext) {
        var unreadObj = NSManagedObject(entity: entityDescriptor, insertInto: managedContext)
        unreadObj.setValue(unread.iden, forKey: FavoriteParser.idMessage.rawValue)
        update(&unreadObj, withFavorite: unread)
    }
    
    func update(_ unreadObj: inout NSManagedObject, withFavorite unread: Unread) {
        unreadObj.setValue(unread.isUnread, forKey: UnreadParser.isUnread.rawValue)
    }
}

extension UnreadPersistence {
    func toUnread() -> Unread {
        return Unread(iden: Int(self.idMessage),
                        isUnread: self.isUnread)
    }
}
