//
//  CoreDataMessagePersistence.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class  CoreDataConfigurator {
    static let shared: CoreDataConfigurator = CoreDataConfigurator()
    
    func getViewContext() -> NSManagedObjectContext? {
        guard
            let appDelegate: AppDelegate = UIApplication.shared.delegate as? AppDelegate
        else {
            return nil
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        return managedContext
    }
}

protocol CoreDataProtocol {
    associatedtype Object
    associatedtype CoreDataEntity
    
    var managedContext: NSManagedObjectContext? { get }
}

enum CoreDataTable: String {
    case comment = "CommentPersistence"
    case message = "MessagePersistence"
    case user = "UserPersistence"
    case favorite = "FavoritePersistence"
    case unread = "UnreadPersistence"
}

extension CoreDataProtocol  {
    var managedContext: NSManagedObjectContext? {
        return CoreDataConfigurator.shared.getViewContext()
    }
}

extension CoreDataProtocol where CoreDataEntity == NSManagedObject {
    func retrieveAll(inside: CoreDataTable) -> [CoreDataEntity] {
        return retrieve(byPredicate: nil, inside: inside)
    }
    
    func retrieve(byPredicate predicate: NSPredicate?, inside: CoreDataTable) -> [CoreDataEntity] {
        guard let managedContext = self.managedContext else { return [] }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: inside.rawValue)
        if let predicate = predicate {
            fetchRequest.predicate = predicate
        }
        
        do {
            return try managedContext.fetch(fetchRequest) as? [CoreDataEntity] ?? []
        } catch let error as NSError {
            print("[ERROR] \(#function) \(error.description)")
        }
        
        return []
    }
    
    func removeAll(inside: CoreDataTable) {
        remove(byPredicate: nil, inside: inside)
    }
    
    func remove(byPredicate predicate: NSPredicate? , inside: CoreDataTable){
        guard let managedContext = self.managedContext else { return }
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: inside.rawValue)
        if let predicate = predicate {
            fetchRequest.predicate = predicate
        }
        
        do {
            let retrieveData = try managedContext.fetch(fetchRequest) as? [CoreDataEntity]
            retrieveData?.forEach { managedContext.delete($0) }
        } catch let error as NSError {
            print("[ERROR] \(#function) \(error.description)")
        }
    }
}
