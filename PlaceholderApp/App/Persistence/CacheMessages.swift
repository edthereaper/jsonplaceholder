//
//  CacheMessages.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

protocol CacheMessages {
    func retriveMessages() -> [Message]
    func retriveMessages(byMessageId messageId: Int) -> [Message]
    func saveOrUpdate(message: Message)
    func dropMessages()
    func removeMessage(withIden iden: Int)
}
