//
//  CacheUnread.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

protocol CacheUnread {
    func getUnreadStatus(byMessageId messageId: Int) -> Unread?
    func saveOrUpdate(unread: Unread)
}
