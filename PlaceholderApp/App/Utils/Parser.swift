//
//  Parser.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

protocol Key {}

enum MessagesParser: String {
    case userId
    case id
    case title
    case body
    case isFavorite
    case isUnread
}

enum UserParser: String {
    case id
    case name
    case phone
    case email
    case website
}

enum CommentParser: String {
    case postId
    case id
    case name
    case email
    case body
}

enum FavoriteParser: String {
    case idMessage
    case isFavorite
}

enum UnreadParser: String {
    case idMessage
    case isUnread
}

extension Dictionary where Key: ExpressibleByStringLiteral {
    subscript<Index: RawRepresentable>(index: Index) -> Value? where Index.RawValue == String {
        get {
            return self[index.rawValue as! Key]
        }
        
        set {
            self[index.rawValue as! Key] = newValue
        }
    }
}

protocol Parser {
    associatedtype Value
    associatedtype Attribute
    
    func parse(responseData: Attribute) -> Value
}
