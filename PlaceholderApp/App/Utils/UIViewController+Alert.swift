//
//  UIViewController + Alert.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/10/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation
import UIKit

struct AlertViewModel {
    let title: String
    let message: String
}

extension AlertViewModel {
    static let notInternetConnection = AlertViewModel(title: "Connection",
                                                      message: "Please connect to internet")
}

extension UIViewController {
    func alert(withViewModel viewModel: AlertViewModel) {
        let alertController = UIAlertController(title: viewModel.title, message: viewModel.message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
