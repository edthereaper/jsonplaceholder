//
//  GetUnreadStatus.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

protocol GetUnreadStatus {
    func execute(messageId: Int) -> Unread
    func execute(messageId: Int, andIndex index: Int) -> Unread
}
