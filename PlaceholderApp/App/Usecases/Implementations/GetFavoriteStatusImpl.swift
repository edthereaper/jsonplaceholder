//
//  GetFavoriteStatusImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

final class GetFavoriteStatusImpl: GetFavoriteStatus, LocatorDependency {
    struct Dependencies: LocatorInjector {
        let cacheFavorite: CacheFavorites = inject()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(messageId: Int) -> Favorite {
        return dependencies.cacheFavorite.getFavoriteStatus(byMessageId: messageId) ?? Favorite(iden: messageId, isFavorite: false)
    }
}
