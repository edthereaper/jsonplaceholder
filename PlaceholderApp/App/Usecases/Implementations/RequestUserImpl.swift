//
//  RequestUserImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/10/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

final class RequestUserImpl: RequestUser, LocatorDependency {
    struct Dependencies: LocatorInjector { }
    
    let dependencies: Dependencies
    let request = Request()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(userId: Int, completion: RequestUserCallback) {
        request.get(url: "https://jsonplaceholder.typicode.com/users?id=\(userId)") { [weak self] (status, data) in
            guard let strongSelf = self else {
                return
            }
            switch status {
            case .success:
                let parseData = strongSelf.parse(responseData: data)
                completion?(parseData)
                break
            default:
                break
            }
        }
    }
}

extension RequestUserImpl: Parser {
    typealias Value = [User]
    typealias Attribute = [[String: AnyObject]]
    
    func parse(responseData: [[String: AnyObject]]) -> [User] {
        var arr: [User] = []
        
        responseData.forEach {
            let userId = $0[UserParser.id] as? Int ?? 0
            let name = $0[UserParser.name] as? String ?? ""
            let phone = $0[UserParser.phone] as? String ?? ""
            let email = $0[UserParser.email] as? String ?? ""
            let website = $0[UserParser.website] as? String ?? ""
            
            let parserObj = User(idUser: userId,
                                      name: name,
                                      email: email,
                                      phone: phone,
                                      website: website)
            
            arr.append(parserObj)
        }
        
        return arr
    }
}
