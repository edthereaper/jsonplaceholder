//
//  RequestCommentsImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/10/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

final class RequestCommmentsImpl: RequestComment, LocatorDependency {
    struct Dependencies: LocatorInjector { }
    
    let dependencies: Dependencies
    let request = Request()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(idMessage: Int, completion: RequestCommnetCallback) {
        request.get(url: "https://jsonplaceholder.typicode.com/comments?postId=\(idMessage)") { [weak self] (status, data) in
            guard let strongSelf = self else {
                return
            }
            switch status {
            case .success:
                let parseData = strongSelf.parse(responseData: data)
                completion?(parseData)
                break
            default:
                break
            }
        }
    }
}

extension RequestCommmentsImpl: Parser {
    typealias Value = [Comment]
    typealias Attribute = [[String: AnyObject]]
    
    func parse(responseData: [[String: AnyObject]]) -> [Comment] {
        var arr: [Comment] = []
        
        responseData.forEach {
            let idMessage = $0[CommentParser.id] as? Int ?? 0
            let idComment = $0[CommentParser.postId] as? Int ?? 0
            let name = $0[CommentParser.name] as? String ?? ""
            let email = $0[CommentParser.email] as? String ?? ""
            let body = $0[CommentParser.body] as? String ?? ""
            
            let parserObj = Comment.init(idMessage: idMessage,
                                         idComment: idComment,
                                         name: name,
                                         email: email,
                                         body: body)
            
            arr.append(parserObj)
        }
        
        return arr
    }
}
