//
//  RequestMessage.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

final class RequestMessagesImpl: RequestMessages, LocatorDependency {
    struct Dependencies: LocatorInjector {
        var cacheMessages: CacheMessages = inject()
        var getUnread: GetUnreadStatus = inject()
        var getFavorite: GetFavoriteStatus = inject()
    }
    
    let dependencies: Dependencies
    let request = Request()
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(completion: RequestMessagesCallback) {
        request.get(url: "https://jsonplaceholder.typicode.com/posts") { [weak self] (status, data) in
            guard let strongSelf = self else {
                return
            }
            switch status {
            case .success:
                let parseData = strongSelf.parse(responseData: data)
                completion?(parseData)
                break
            default:
                break
            }
        }
    }
}

extension RequestMessagesImpl: Parser {
    typealias Value = [Message]
    typealias Attribute = [[String: AnyObject]]
    
    func parse(responseData: [[String: AnyObject]]) -> [Message] {
        var arr: [Message] = []
        
        for (index, message) in responseData.enumerated() {
            let userId = message[MessagesParser.userId] as? Int ?? 0
            let iden = message[MessagesParser.id] as? Int ?? 0
            let title = message[MessagesParser.title] as? String ?? ""
            let body = message[MessagesParser.body] as? String ?? ""
            let favorite = dependencies.getFavorite.execute(messageId: iden)
            let unread = dependencies.getUnread.execute(messageId: iden, andIndex: index)
            
            let parserObj = Message(userId: userId,
                                              iden: iden,
                                              title: title,
                                              body: body,
                                              favorite: favorite,
                                              unread: unread)
            
            arr.append(parserObj)
        }
        
        return arr
    }
}
