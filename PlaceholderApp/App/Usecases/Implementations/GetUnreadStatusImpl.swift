//
//  GetUnreadStatusImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

final class GetUnreadStatusImpl: GetUnreadStatus, LocatorDependency {
    struct Constants {
        static let minUnread = 20
    }
    
    struct Dependencies: LocatorInjector {
        let cacheUnread: CacheUnread = inject()
        let setUnread: SetUnreadStatus = inject()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(messageId: Int) -> Unread {
        return dependencies.cacheUnread.getUnreadStatus(byMessageId: messageId) ?? Unread(iden: messageId, isUnread: false)
    }
    
    func execute(messageId: Int, andIndex index: Int) -> Unread {
        guard let unread = dependencies.cacheUnread.getUnreadStatus(byMessageId: messageId) else {
            let unreadStatus = index < Constants.minUnread ? true: false
            dependencies.setUnread.execute(messageId: messageId, isUnread: unreadStatus)
            return Unread(iden: messageId, isUnread: unreadStatus)
        }
            
        return unread
    }
}
