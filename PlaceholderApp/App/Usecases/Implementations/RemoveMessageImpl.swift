//
//  RemoveMessageImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

class RemoveMessageImpl: RemoveMessages, LocatorDependency {
    struct Dependencies: LocatorInjector {
        let cacheUnread: CacheUnread = inject()
        let cacheFavorite: CacheFavorites = inject()
        let cacheMessage: CacheMessages = inject()
    }
    
    let dependencies: Dependencies
    
    required init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(message: Message) {
        dependencies.cacheMessage.removeMessage(withIden: message.iden)
    }
    
    func execute(messages: [Message]) {
        dependencies.cacheMessage.dropMessages()
    }
}
