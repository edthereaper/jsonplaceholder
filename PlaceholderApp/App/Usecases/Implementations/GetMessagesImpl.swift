//
//  GetMessagesImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

final class GetMessagesImpl: GetMessages, LocatorDependency{
    struct Dependencies: LocatorInjector {
        var requestMessages: RequestMessages = inject()
        var cacheMessages: CacheMessages = inject()
        var getUnread: GetUnreadStatus = inject()
        var getFavorite: GetFavoriteStatus = inject()
    }
    
    let dependencies: Dependencies
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(completion: GetMessagesCallback) {
        let retrivedMessages = dependencies.cacheMessages.retriveMessages()
        for var message in retrivedMessages {
            message.favorite = dependencies.getFavorite.execute(messageId: message.iden)
            message.unread = dependencies.getUnread.execute(messageId: message.iden)
        }
        completion?(retrivedMessages)
        dependencies.requestMessages.execute { [weak self] (messages) in
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                messages.forEach{
                    let message = $0
                    strongSelf.dependencies.cacheMessages.saveOrUpdate(message: message)
                }
                completion?(messages)
            }
        }
    }
    
    func execute(messageId: Int) -> Message? {
        return dependencies.cacheMessages.retriveMessages(byMessageId: messageId).first
    }
}

