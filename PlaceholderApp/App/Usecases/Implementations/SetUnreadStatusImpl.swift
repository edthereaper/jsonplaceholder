//
//  SetUnreadStatusImpl.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

final class SetUnreadStatusImpl: SetUnreadStatus, LocatorDependency {
    struct Dependencies: LocatorInjector {
        let cacheUnread: CacheUnread = inject()
    }
    
    let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    func execute(messageId: Int, isUnread: Bool) {
        let unreadData = Unread(iden: messageId, isUnread: isUnread)
        dependencies.cacheUnread.saveOrUpdate(unread: unreadData)
    }
}
