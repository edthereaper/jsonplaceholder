//
//  SetFavoriteMessage.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

protocol SetFavoriteStatus {
    func execute(messageId: Int, isFavorite: Bool)
}
