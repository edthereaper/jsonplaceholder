//
//  RequestComments.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/10/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

typealias RequestCommnetCallback = (([Comment]) -> Void)?

protocol RequestComment {
    func execute(idMessage: Int, completion: RequestCommnetCallback)
}
