//
//  GetMessages.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/9/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

typealias GetMessagesCallback = (([Message]) -> Void)?

protocol GetMessages {
    func execute(completion: GetMessagesCallback) 
}
