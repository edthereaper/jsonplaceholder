//
//  RequestUser.swift
//  PlaceholderApp
//
//  Created by Eduardo Rodriguez on 3/10/19.
//  Copyright © 2019 Eduardo Rodriguez. All rights reserved.
//

import Foundation

typealias RequestUserCallback = (([User]) -> Void)?

protocol RequestUser {
    func execute(userId: Int, completion: RequestUserCallback)
}
