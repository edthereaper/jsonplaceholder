# JSONPlaceHolder

### About
JSONPlaceHolder is an app that list all messages and details from:
[https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/)

### How-To
1. Clone this repository in your mac
1. Open Xcode app
2. just tap run / play button :)

### Requeriments
1. Load the posts​ from the JSON API and populate the sidebar.
2. The first 20 posts should have a blue dot indicator.
3. Remove the blue dot indicator once the related post is read.
4. Once a post is touched, its related content is shown in the main content area.
5. The related content also displays the user information.
6. Add a button in the navigation. It adds the current post to favorites.
7. Each cell should have the functionality to swipe and delete the post.
8. Add a button to the footer that removes all posts.
9. Add a button to navigation that reloads all posts.
10. Add a segmented control to filter posts (All / Favorites)
11. Favorite posts should have a star indicator.

### Arquitectures Proposed
1. Clean Arquitecture
2. Dependency injection

### Third Party Libraries
This project dont use any third party libraries, all code was wrote using swift and use standards libraries.

### Some Extras
1. Cache all posts (You could use Core Data (iOS), Realm or what you consider is the best
tool to handle persistency).
2. Add animations when the user deletes each/all posts.
3. Show a list of comments related to each post. It should be located in the main content
area.
4. ~~Add unit testing as you consider it.~~


### Screenshot
![alt text](https://gitlab.com/edthereaper/jsonplaceholder/raw/master/Screenshots/Image1.png)

Thanks :)
